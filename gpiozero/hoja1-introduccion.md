# CamJam EduKit de robótica - Introducción

Proyecto: Configurar sus Raspberry Pi

Descripción: Configurar sus Raspberry Pi y ejecutar su primer programa en
Python para imprimir «Hello World» en la pantalla. No van a conectar ningún
contenido del CamJam EduKit al Raspberry Pi para este ejercicio.

## Las hojas de trabajo del CamJam EduKit 3

El CamJam EduKit 3 - robótica es el tercer kit en la serie de CamJam EduKit. No
tienen que haber usado los primeros dos kits para poder usar este, aunque
dependiendo de su conocimiento de la Raspberry Pi, sus pines GPIO («General
Purpose Input/Output) y de electrónica, podrían encontrar útil leer las hojas
de trabajo anteriores para entender algunos de los conceptos más básicos.
Pueden descargar las hojas de trabajo, de forma gratuita, en
[http://camjam.me/edukit](http://camjam.me/edukit).

Los EduKits son compatibles con todos los sabores de la Raspberry Pi - Modelos
A, B, A+, B+, Pi2, Pi3, Pi3B+ y PiZero.

## Notas y suposiciones

A lo largo de estas hojas de trabajo, se asume que están usando Raspbian
(Stretch), y que están editando su código con los editores Python 3 IDLE o
Thonny.

En esta versión de las hojas de trabajo estarán usando la biblioteca de Python
3 GPIO Zero para interactuar con los pines GPIO de la Raspberry Pi.

Si están viendo estas hojas de trabajo en sus Raspberry Pi, no deberían copiar
y pegar el código de las hojas de trabajo, ya que el espaciado al inicio de las
líneas no necesariamente será pegado de forma correcta y el código no siempre
funcionará; la indentación al inicio del código de Python es importante - es la
forma de Python de reconocer cómo se debería agrupar el código en
«condiciones», «ciclos» y «funciones». En lugar de esto, escríbanlo o
descárguenlo de GitHub con las instrucciones al final de esta hoja de trabajo.

## Equipo requerido

Para esta hoja de trabajo requerirán:

* Una Raspberry Pi.
* Una tarjeta SD para poner en su versión de la Pi (recomendados 8GB) con
  Raspbian Stretch con un sistema operativo de escritorio en ella. Las
  instrucciones para configurar Raspbian se pueden encontrar en el
  [sitio web de Raspberry Pi](https://www.raspberrypi.org/downloads/).
* Un monitor y un cable para conectar a la salida HDMI o de vídeo compuesto
  de sus Pi.
* Un teclado y un ratón.
* Un cable de poder de Raspberry Pi.
* El kit EduKit 3, disponible en [The Pi Hut](https://thepihut.com/edukit)

## Configurar sus Raspberry Pi

Encuentren sus Raspberry Pi.

* Conecten la tarjeta microSD (o SD en los modelos originales A y B).
* Conecten el cable de HDMI/vídeo a la Pi y al monitor.
* Conecten el teclado y el ratón a los puertos USB. Necesitarán un adaptador
  si están usando una Pi Zero, y un hub USB si sólo tienen un puerto USB
  disponible.
* Conecten el cable de poder.

// TODO imagen.

Cuando todo esté conectado, se debería ver como en la imagen de arriba. Las
A+, B+, Pi2, Pi3 y PiZero se verán un poco diferente.

## Actualizar Raspbian

Es bueno mantener el sistema operativo de sus Raspberry Pi actualizado con los
arreglos y mejoras más recientes. Sólo pueden hacer esto si sus Raspberry Pi
están conectadas a la internet. Podría tomar un poco de tiempo (tal vez hasta
una hora), así que sólo deberían hacer esto cuando tengan ese tiempo.

Para actualizar Raspbian, abran una termina ya sea haciendo clic en el icono
de la barra de menú que se ve como así:

// TODO imagen

O escogiendo Accesorios -> Terminal en la barra de menú de arriba.

Escriba los siguientes dos comandos, uno después del otro, dejando que cada
comando termine antes de empezar el siguiente:

```
sudo apt-get update
sudo apt-get upgrade
```

## Escribir código

Ahora van a crear su primer pieza pequeña de código de Python que simplemente
imprimirá «Hello World» en la pantalla.

Primero van a crear un directorio donde almacenarán el código para las hojas de
trabajo de EduKit. Abran el «Administrador de Archivos» de la barra de menú:

// TODO imagen

Debería iniciar en su directorio de «inicio». Creen un nuevo directorio
navegando el menú del Administrador de Archivos en Archivo -> Crear Nuevo...
-> Directorio. Escriban «Edukit3» en la ventana emergente y presionen OK.
Ahora pueden cerrar el Administrador de Archivos haciendo clic en la X en la
esquina superior derecha de la ventana.

Abran «Python 3 (IDLE)» o «Thonny Python IDE» del menú (en Programación), y
creen un archivo usando el elemento del menú «Nuevo archivo» de IDLE o «Nuevo»
en el menú de archivo (o usen `Ctrl+N`).

Escriban el siguiente código en su editor preferido de forma exacta como lo
están viendo:

```
# Print Hello World!
print("Hello World!")
```

Todo en la misma línea después de un `#` es un comentario y será ignorado por
Python.

Guarden el archivo en el directorio «EduKit3» que crearon arriba, llamando al
archivo `1-helloworld.py`.

## Ejecutar el código

Para ejecutar su código, selecciones el la opción de menú Ejecutar -> Ejecutar
Módulo, o presionen F5.

Verán «Hello World!» impreso en el «shell» de Python.

## Descargar el código de EduKit de GitHub

El código escrito para el CamJam EduKit y listado en las hojas de trabajo
también está almacenado en GitHub. Sigan las siguientes instrucciones para
descargar todo el código de EduKit.

### Repositorio de GitHub

Todos los repositorios de cada EduKit pueden encontrarse en línea en
[https://github.com/CamJam-EduKit](https://github.com/CamJam-EduKit). Podrían
descargar archivos individuales o los repositorios enteros de ahí.

## Instalar Git

Antes de que puedan «clonar» el código de GitHub, deben asegurar que la
herramienta `git` esté instalada en sus Raspberry Pi. Para hacer esto, sus
Raspberry Pi deben estar conectados a internet. Abran una ventana de terminal
y primero actualicen el repositorio Pi usando:

```
sudo apt-get update
```

Luego instalen `git` usando:

```
sudo apt-get install git-core
```

### Descargando en el Raspberry Pi

El código de EduKit y las hojas de trabajo pueden ser descargados usando el
siguiente comando:

```
cd ~
git clone git://github.com/CamJam-EduKit/EduKit3.git
```

El código será almacenado en el subdirectorio `Code` en directorio
`EduKit3/CamJam Edukit 3 - GPIO Zero`.
